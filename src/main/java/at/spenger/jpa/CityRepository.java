package at.spenger.jpa;

import org.springframework.data.repository.CrudRepository;

public interface CityRepository extends CrudRepository<City, Integer>{

}
